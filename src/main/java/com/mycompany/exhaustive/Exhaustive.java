/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.mycompany.exhaustive;

/**
 *
 * @author User
 */
public class Exhaustive {

    public int maxProfit(int[] prices) {
        int n = prices.length, maxProfitSoFar = 0;
        // maxIncDiffSum[i] max sum of increasing difference ending at prices[i].
        int[] maxIncDiffSum = new int[n];
        
        for (int i = 1; i < n; i++) {
            maxIncDiffSum[i] = Math.max(0, maxIncDiffSum[i - 1] + prices[i] - prices[i - 1]);
            maxProfitSoFar = Math.max(maxProfitSoFar, maxIncDiffSum[i]);
        }
        
        return maxProfitSoFar;
    }
    }
